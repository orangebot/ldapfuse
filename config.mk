#LDAPFUSE Version
VERSION = 1.0

#Paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

#Includes and Libs
INCS = -I$(shell find /usr/include/ -name fuse | head -n1) \
       -I$(shell find /usr/include/ -name libHX | head -n1)
LIBS = -lHX \
       -lfuse \
       -llber \
       -lldap \
       -pthread

#Flags
CPPFLAGS = -DPACKAGE=\"ldapfuse\" \
           -DVERSION=\"$(VERSION)\" \
           -D_FILE_OFFSET_BITS=64 \
           -D_XOPEN_SOURCE=600
BUILDCFLAGS = $(INCS) $(CPPFLAGS) $(CFLAGS)
BUILDLDFLAGS = $(LIBS) $(LDFLAGS)

# compiler and linker
CC = gcc
