#LDAPFUSE - Look at LDAP structures as files using fuse.
#Please see the file LICENSE.GPL3 for copyright and license details.

.POSIX:

include config.mk

SRC = mount.c
OBJ = $(SRC:.c=.o)

all: options ldapfuse

options:
	@echo ldapfuse build options:
	@echo "CFLAGS  = $(BUILDCFLAGS)"
	@echo "LDFLAGS = $(BUILDLDFLAGS)"
	@echo "CC      = $(CC)"

$(OBJ): $(SRC)
	$(CC) $(BUILDCFLAGS) -c $<

ldapfuse: $(OBJ)
	$(CC) -o $@ $(OBJ) $(BUILDLDFLAGS)

clean:
	rm -f ldapfuse $(OBJ)

install: ldapfuse
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f st $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/ldapfuse
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < ldapfuse.1 > $(DESTDIR)$(MANPREFIX)/man1/ldapfuse.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/ldapfuse.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/ldapfuse
	rm -f $(DESTDIR)$(MANPREFIX)/man1/ldapfuse.1

.PHONY: all options clean install uninstall