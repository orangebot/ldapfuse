/*
 *	LDAP virtual filesystem view
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2009-2011
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation, version 3.
 */
#define FUSE_USE_VERSION 26
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fuse.h>
#include <fuse_opt.h>
#define LDAP_DEPRECATED 1
#include <lber.h>
#include <ldap.h>
#include <libHX/defs.h>
#include <libHX/deque.h>
#include <libHX/init.h>
#include <libHX/map.h>
#include <libHX/misc.h>
#include <libHX/option.h>
#include <libHX/string.h>

struct ldap_namei {
	char *dn, *attr;
};

struct ldapfuse_dir {
	LDAPMessage *base_result;
	LDAPMessage *one_result;
};

/**
 * @value:	contents
 * @length:	number of bytes in the @value memory block
 * @otime:	creation time of this entry
 *
 * The ldapfuse_cache stores the last constructed file content, so that it
 * needs not be built again. The motivating factor was needing to answer fstat
 * and the read.
 */
struct ldapfuse_cache_object {
	char *value;
	size_t length;
	time_t otime;
};

static struct HXmap *ldapfuse_cache_pool;
static pthread_mutex_t ldapfuse_cache_lock;
static time_t ldapfuse_cache_timeout = 5;

static pthread_mutex_t ldapfuse_net_lock;
static unsigned int ldapfuse_foreground;
static const char *ldapfuse_basedn;
static char *ldapfuse_password;
static char *ldapfuse_binddn;
static struct HXdeque *ldapfuse_extra_opts;
static LDAP *ldapfuse_conn;
static uid_t ldapfuse_uid;
static gid_t ldapfuse_gid;

static int ldapfuse_errno(int err)
{
	switch (err) {
	case LDAP_NO_SUCH_OBJECT:
	case LDAP_INVALID_DN_SYNTAX:
		return -ENOENT;
	case LDAP_DECODING_ERROR:
		return -EIO;
	default:
		printf("Unhandled LDAP error code %d\n", err);
		printf("LDAP %s\n", ldap_err2string(err));
		return -EIO;
	}
}

static char *ldapfuse_rdn2path(char *dn)
{
	unsigned int esc = 0;
	const char *i;
	char *rdn, *o;

	for (o = dn; *o != '\0'; ++o)
		if (*o == '\\' || *o == '/') {
			++esc;
		} else if (*o == ',') {
			*o = '\0';
			break;
		}

	rdn = malloc(strlen(dn) - esc + 4 * esc + 1);
	if (rdn == NULL)
		goto out;
	for (i = dn, o = rdn; *i != '\0'; ++i) {
		switch (*i) {
		case '\\':
		case '/':
			esc = *i;
			*o++ = '\\';
			*o++ = '0' + esc / 256;
			*o++ = '0' + esc / 8 % 8;
			*o++ = '0' + esc % 8;
			break;
		default:
			*o++ = *i;
			break;
		}
	}
	*o = '\0';
 out:
	ldap_memfree(dn);
	return rdn;
}

static char *ldapfuse_fsname_for_fuse(const char *orig)
{
	unsigned int z = 0;
	const char *i;
	char *out, *o;

	for (i = orig; *i != '\0'; ++i, ++z)
		if (*i == '\\' || *i == ',')
			++z;

	o = out = malloc(z + 1);
	if (out == NULL)
		return NULL;

	for (i = orig; *i != '\0'; ++i) {
		if (*i == '\\' || *i == ',')
			*o++ = '\\';
		*o++ = *i;
	}
	*o = '\0';
	return out;
}

/**
 * Unexpand an "\xxx" octal sequence in the pathname (usually \057, '/')
 * back to the character verbatim.
 */
static void ldapfuse_path2rdn(char *s)
{
	const char *i = s;
	char *o = s;

	while (*i != '\0') {
		if (*i != '\\') {
			*o++ = *i++;
			continue;
		}
		++i;
		*o    = (*i++ - '0') * 64;
		*o   |= (*i++ - '0') * 8;
		*o++ |= *i++ - '0';
	}
	*o = '\0';
}

static int ldapfuse_namei_lookup(struct ldap_namei *leaf, const char *path)
{
	unsigned int i;
	int fields = 0;
	char **comp;

	leaf->attr = NULL;
	if (*path == '/')
		++path;
	if (*path == '\0') {
		/* Root entry */
		leaf->dn = HX_strdup(ldapfuse_basedn);
		if (leaf->dn == NULL)
			return -errno;
		return 0;
	}

	comp = HX_split(path, "/", &fields, 0);
	if (comp == NULL)
		return -errno;

	leaf->attr = NULL;
	leaf->dn = malloc(strlen(path) + 1 + strlen(ldapfuse_basedn) + 1);
	if (leaf->dn == NULL)
		goto out;

	*leaf->dn = '\0';
	for (i = 0; i < fields; ++i)
		if (strchr(comp[i], '=') == NULL)
			/*
			 * Assume that a path component without a '=' is an
			 * attribute name.
			 */
			break;
	if (i < fields) {
		ldapfuse_path2rdn(comp[i]);
		leaf->attr = HX_strdup(comp[i]);
		if (leaf->attr == NULL)
			goto out;
	}
	while (i > 0) {
		--i;
		ldapfuse_path2rdn(comp[i]);
		strcat(leaf->dn, comp[i]);
		if (i > 0)
			strcat(leaf->dn, ",");
	}
	if (*ldapfuse_basedn != '\0') {
		if (*leaf->dn != '\0')
			strcat(leaf->dn, ",");
		strcat(leaf->dn, ldapfuse_basedn);
	}
	HX_zvecfree(comp);
	return 1;

 out:
	free(leaf->dn);
	free(leaf->attr);
	HX_zvecfree(comp);
	return -errno;
}

static void ldapfuse_namei_free(struct ldap_namei *leaf)
{
	free(leaf->dn);
	free(leaf->attr);
}

static void ldapfuse_namei_hfree(void *leaf)
{
	ldapfuse_namei_free(leaf);
}

/**
 * Free a cache object. This is called by pthread at the end of a thread's
 * lifetime.
 */
static void ldapfuse_cache_free(void *datap)
{
	struct ldapfuse_cache_object *obj = datap;

	free(obj->value);
	free(obj);
}

static void *ldapfuse_namei_clone(const void *ptr, size_t len)
{
	const struct ldap_namei *in = ptr;
	struct ldap_namei *out;

	out = malloc(sizeof(*out));
	if (out == NULL)
		return NULL;
	out->dn   = HX_strdup(in->dn);
	out->attr = HX_strdup(in->attr);
	if ((in->dn != NULL && out->dn == NULL) ||
	    (out->dn != NULL && out->attr == NULL)) {
		free(out->dn);
		free(out->attr);
		free(out);
		return NULL;
	}
	return out;
}

static unsigned long ldapfuse_namei_hash(const void *p, size_t len)
{
	const struct ldap_namei *leaf = p;
	unsigned long h[2];

	h[0] = HXhash_jlookup3s(leaf->dn, 0);
	h[1] = HXhash_jlookup3s(leaf->attr, 0);
	return HXhash_jlookup3(h, sizeof(h));
}

/**
 * Compare two LDAP tree path specifications and decide on whether they are
 * equal. Returns 0 for equality, otherwise for inequality. Despite strcmp is
 * used, no assumptions about the meaning of a negative <0 or a positive >0
 * return value of ldapfuse_namei_cmp shall be made.
 */
static int ldapfuse_namei_cmp(const void *va, const void *vb, size_t len)
{
	const struct ldap_namei *a = va, *b = vb;
	int ret;

	if (a->dn == NULL || b->dn == NULL ||
	    a->attr == NULL || b->attr == NULL)
		return -1;
	ret = strcmp(a->dn, b->dn);
	if (ret != 0)
		return ret;
	return strcmp(a->attr, b->attr);
}

/**
 * @obj:	cache object to populate
 * @values:	array of attributes from the LDAP result
 *
 * Concatenates all attributes in the @values array and puts the result in
 * the specified cache object.
 */
static int ldapfuse_cache_fill2(struct ldapfuse_cache_object *obj,
    struct berval *const *values)
{
	struct berval *const *valueptr;
	char *new_value;
	int wlen;

	obj->length = 0;
	for (valueptr = values; *valueptr != NULL; ++valueptr)
		obj->length += (*valueptr)->bv_len + 1;

	new_value = realloc(obj->value, obj->length + 1);
	if (new_value == NULL)
		return -errno;
	obj->value = new_value;

	for (wlen = 0, valueptr = values; *valueptr != NULL; ++valueptr) {
		memcpy(obj->value + wlen, (*valueptr)->bv_val,
		       (*valueptr)->bv_len);
		wlen += (*valueptr)->bv_len;
		obj->value[wlen++] = '\n';
	}
	obj->value[wlen] = '\0';
	return 1;
}

/**
 * @obj:	cache object to fill
 * @result:	LDAP result message to interpret
 * @leaf:	leaf that is being accessed
 *
 * Decode the LDAP response and populate the cache object.
 */
static int ldapfuse_cache_fill(struct ldapfuse_cache_object *obj,
    LDAPMessage *result, const struct ldap_namei *leaf)
{
	struct berval **values;
	int ret;

	values = ldap_get_values_len(ldapfuse_conn, result, leaf->attr);
	pthread_mutex_unlock(&ldapfuse_net_lock);
	if (values == NULL)
		return ldapfuse_errno(LDAP_DECODING_ERROR);

	ret = ldapfuse_cache_fill2(obj, values);
	ldap_value_free_len(values);
	if (ret <= 0)
		return ret;

	pthread_mutex_lock(&ldapfuse_cache_lock);
	/*
	 * If the leaf was already known, it will be overwritten -
	 * and quite atomic so.
	 */
	ret = HXmap_add(ldapfuse_cache_pool, leaf, obj);
	pthread_mutex_unlock(&ldapfuse_cache_lock);
	if (ret < 0) {
		fprintf(stderr, "%s: HXmap_add: %s\n",
		        __func__, strerror(-ret));
		return ret;
	}
	return 1;
}

/**
 * @objp:	result pointer
 * @leaf:	leaf to access
 *
 * Build and execute the LDAP search query for the chosen leaf, and cache it.
 * This is called from ldapfuse_getattr and ldapfuse_read.
 */
static int ldapfuse_namei_cache(struct ldapfuse_cache_object **objp,
    const struct ldap_namei *leaf)
{
	struct ldapfuse_cache_object *cache_obj;
	LDAPMessage *result;
	char *attrs[2];
	int ret;

	pthread_mutex_lock(&ldapfuse_cache_lock);
	cache_obj = HXmap_get(ldapfuse_cache_pool, leaf);
	pthread_mutex_unlock(&ldapfuse_cache_lock);
	if (cache_obj != NULL &&
	    cache_obj->otime + ldapfuse_cache_timeout >= time(NULL)) {
		*objp = cache_obj;
		return 0;
	}

	cache_obj = calloc(1, sizeof(*cache_obj));
	if (cache_obj == NULL)
		return -errno;

	cache_obj->otime = time(NULL);
	attrs[0] = leaf->attr;
	attrs[1] = NULL;
	pthread_mutex_lock(&ldapfuse_net_lock);
	ret = ldap_search_ext_s(ldapfuse_conn, leaf->dn, LDAP_SCOPE_BASE,
	      NULL, attrs, false, NULL, NULL, NULL, 1, &result);
	if (ret != LDAP_SUCCESS || result == NULL) {
		pthread_mutex_unlock(&ldapfuse_net_lock);
		ret = ldapfuse_errno(ret);
	} else {
		/*
		 * mutex unlocked in fill, after ldapfuse_conn
		 * is no longer used.
		 */
		ret = ldapfuse_cache_fill(cache_obj, result, leaf);
		if (ret > 0)
			*objp = cache_obj;
	}
	ldap_msgfree(result);
	if (ret <= 0)
		free(cache_obj);
	return ret;
}

static const struct HXmap_ops ldapfuse_cache_ops = {
	.k_hash    = ldapfuse_namei_hash,
	.k_compare = ldapfuse_namei_cmp,
	.k_clone   = ldapfuse_namei_clone,
	.k_free    = ldapfuse_namei_hfree,
	.d_free    = ldapfuse_cache_free,
};

static int ldapfuse_init(const char *uridn)
{
	char *uri, *p;
	int ret;

	ldapfuse_uid = geteuid();
	ldapfuse_gid = getegid();

	pthread_mutex_init(&ldapfuse_net_lock, NULL);
	pthread_mutex_init(&ldapfuse_cache_lock, NULL);
	ldapfuse_cache_pool = HXmap_init5(HXMAPT_DEFAULT, 0,
		&ldapfuse_cache_ops, sizeof(struct ldap_namei),
		sizeof(struct ldapfuse_cache_object));
	if (ldapfuse_cache_pool == NULL) {
		perror("HXmap_init");
		return -errno;
	}

	uri = HX_strdup(uridn);
	if (uri == NULL) {
		perror("HX_strdup");
		return -errno;
	}

	p = strstr(uri, "://");
	p = (p != NULL) ? p + 3 : uri;
	p = strchr(p, '/');
	if (p != NULL) {
		*p = '\0';
		ldapfuse_basedn = p + 1;
	}

	ret = ldap_initialize(&ldapfuse_conn, uri);
	if (ret != LDAP_SUCCESS)
		return ldapfuse_errno(ret);

	ret = LDAP_VERSION3;
	ldap_set_option(ldapfuse_conn, LDAP_OPT_PROTOCOL_VERSION, &ret);
	ret = LDAP_MAXINT;
	ldap_set_option(ldapfuse_conn, LDAP_OPT_SIZELIMIT, &ret);
	ldap_start_tls_s(ldapfuse_conn, NULL, NULL);
	ret = ldap_simple_bind_s(ldapfuse_conn, ldapfuse_binddn, ldapfuse_password);
	if (ret != LDAP_SUCCESS)
		return ldapfuse_errno(ret);
	return 1;
}

static void ldapfuse_exit(void)
{
	pthread_mutex_destroy(&ldapfuse_net_lock);
	pthread_mutex_destroy(&ldapfuse_cache_lock);
	HXmap_free(ldapfuse_cache_pool);
}

static int ldapfuse_getattr(const char *path, struct stat *sb)
{
	struct ldapfuse_cache_object *obj;
	struct ldap_namei leaf;
	LDAPMessage *result, *entry;
	BerElement *ber = NULL;
	char *attrs[2], *attr;
	int ret;

	ret = ldapfuse_namei_lookup(&leaf, path);
	if (ret < 0)
		return ret;

	attrs[0] = leaf.attr;
	attrs[1] = NULL;
	pthread_mutex_lock(&ldapfuse_net_lock);
	ret = ldap_search_ext_s(ldapfuse_conn, leaf.dn, LDAP_SCOPE_BASE,
	      NULL, (leaf.attr != NULL) ? attrs : NULL, true,
	      NULL, NULL, NULL, 1, &result);
	if (ret != LDAP_SUCCESS || result == NULL ||
	    (entry = ldap_first_entry(ldapfuse_conn, result)) == NULL) {
		ret = ldapfuse_errno(ret);
		pthread_mutex_unlock(&ldapfuse_net_lock);
		goto out;
	}
	attr = ldap_first_attribute(ldapfuse_conn, entry, &ber);
	pthread_mutex_unlock(&ldapfuse_net_lock);
	ber_free(ber, false);
	if (attr == NULL) {
		ret = -ENOENT;
		goto out;
	}
	ldap_memfree(attr);

	if (leaf.attr != NULL) {
		sb->st_mode = S_IFREG | S_IRUGO | S_IRUSR;
		ret = ldapfuse_namei_cache(&obj, &leaf);
		if (ret < 0)
			goto out;
		sb->st_size = obj->length;
	} else {
		sb->st_mode = S_IFDIR | S_IRUGO | S_IXUGO | S_IRUSR;
	}

	sb->st_uid = ldapfuse_uid;
	sb->st_gid = ldapfuse_gid;
	sb->st_nlink = 1;
	ret = 0;
 out:
	ldap_msgfree(result);
	ldapfuse_namei_free(&leaf);
	return ret;

}

static int ldapfuse_opendir(const char *path, struct fuse_file_info *filp)
{
	struct ldapfuse_dir *dir;
	struct ldap_namei leaf;
	int ret;

	dir = malloc(sizeof(*dir));
	if (dir == NULL)
		return -errno;
	if ((ret = ldapfuse_namei_lookup(&leaf, path)) < 0)
		goto out;

	/*
	 * Doing two separate queries so we do not needlessy grab
	 * the attributes of all sub-DNs.
	 */
	pthread_mutex_lock(&ldapfuse_net_lock);
	ret = ldap_search_ext_s(ldapfuse_conn, leaf.dn, LDAP_SCOPE_BASE, NULL,
	      NULL, false, NULL, NULL, NULL, 1, &dir->base_result);
	pthread_mutex_unlock(&ldapfuse_net_lock);
	if (ret != LDAP_SUCCESS) {
		ret = ldapfuse_errno(ret);
		goto out2;
	}

	/* Ignore errors on subobjects */
	pthread_mutex_lock(&ldapfuse_net_lock);
	ldap_search_ext_s(ldapfuse_conn, leaf.dn, LDAP_SCOPE_ONE, NULL,
		NULL, true, NULL, NULL, NULL, 0, &dir->one_result);
	pthread_mutex_unlock(&ldapfuse_net_lock);
	filp->fh = reinterpret_cast(unsigned long, dir);
	ldapfuse_namei_free(&leaf);
	return 0;

 out2:
	ldapfuse_namei_free(&leaf);
 out:
	free(dir);
	return ret;
}

static int ldapfuse_readdir(const char *path, void *what,
    fuse_fill_dir_t filldir, off_t offset, struct fuse_file_info *filp)
{
	struct ldapfuse_dir *dir;
	LDAPMessage *entry;
	BerElement *ber;
	struct stat sb;
	char *attr;
	int ret;

	dir = reinterpret_cast(void *, static_cast(unsigned long, filp->fh));

	memset(&sb, 0, sizeof(sb));
	/* Subdirs first */
	for (entry = ldap_first_entry(ldapfuse_conn, dir->one_result);
	     entry != NULL; entry = ldap_next_entry(ldapfuse_conn, entry))
	{
		char *rdn;

		rdn = ldapfuse_rdn2path(ldap_get_dn(ldapfuse_conn, entry));
		if (rdn == NULL)
			return -ENOMEM;
		sb.st_mode = S_IFDIR | S_IRUGO | S_IXUGO | S_IWUSR;
		ret = (*filldir)(what, rdn, &sb, 0);
		free(rdn);
		if (ret > 0)
			break;
	}

	entry = ldap_first_entry(ldapfuse_conn, dir->base_result);
	for (attr = ldap_first_attribute(ldapfuse_conn, entry, &ber);
	     attr != NULL;
	     attr = ldap_next_attribute(ldapfuse_conn, entry, ber))
	{
		sb.st_mode = S_IFREG | S_IRUGO | S_IWUSR;
		ret = (*filldir)(what, attr, &sb, 0);
		ldap_memfree(attr);
		if (ret > 0)
			break;
	}
	if (ber != NULL)
		ber_free(ber, false);
	return 0;
}

static int ldapfuse_releasedir(const char *path, struct fuse_file_info *filp)
{
	struct ldapfuse_dir *dir;

	dir = reinterpret_cast(void *, static_cast(unsigned long, filp->fh));
	ldap_msgfree(dir->base_result);
	ldap_msgfree(dir->one_result);
	free(dir);
	return 0;
}

static int ldapfuse_open(const char *path, struct fuse_file_info *fi)
{
	struct ldap_namei leaf;
	int ret;

	ret = ldapfuse_namei_lookup(&leaf, path);
	if (ret < 0)
		return ret;
	if (leaf.attr == NULL)
		return -EIO;

	ret = 0;
	ldapfuse_namei_free(&leaf);
	return ret;
}

static int ldapfuse_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct ldapfuse_cache_object *obj;
	struct ldap_namei leaf;
	int ret;

	ret = ldapfuse_namei_lookup(&leaf, path);
	if (ret < 0)
		return ret;
	if (leaf.attr == NULL)
		return -EIO;
	ret = ldapfuse_namei_cache(&obj, &leaf);
	if (ret < 0)
		return ret;
	if (offset < obj->length) {
		if (offset + size > obj->length)
			size = obj->length - offset;
		memcpy(buf, obj->value + offset, size);
	} else {
		size = 0;
	}

	ldapfuse_namei_free(&leaf);
	return size;
}

static const struct fuse_operations ldapfuse_ops = {
	.getattr     = ldapfuse_getattr,
	.opendir     = ldapfuse_opendir,
	.readdir     = ldapfuse_readdir,
	.releasedir  = ldapfuse_releasedir,
	.open        = ldapfuse_open,
	.read        = ldapfuse_read,
};

static bool ldapfuse_get_options(int *argc, const char ***argv)
{
	unsigned int query_for_p = false;
	struct HXoption options_table[] = {
		{.sh = 'D', .type = HXTYPE_STRING, .ptr = &ldapfuse_binddn,
		 .help = "DN for binding"},
		{.sh = 'W', .type = HXTYPE_NONE, .ptr = &query_for_p,
		 .help = "Interactively ask for password"},
		{.sh = 'f', .type = HXTYPE_NONE, .ptr = &ldapfuse_foreground,
		 .help = "Run " PACKAGE " in foreground"},
		{.sh = 'o', .type = HXTYPE_STRDQ, .ptr = ldapfuse_extra_opts,
		 .help = "Additional FUSE options for the vfsmount"},
		{.sh = 'w', .type = HXTYPE_STRING, .ptr = &ldapfuse_password,
		 .help = "Bind password"},
		HXOPT_AUTOHELP,
		HXOPT_TABLEEND,
	};

	if (HX_getopt(options_table, argc, argv, HXOPT_USAGEONERR) !=
	    HXOPT_ERR_SUCCESS) {
		const char **p;
		fprintf(stderr, "Command-line was:");
		for (p = (*argv); *p != NULL; ++p)
			fprintf(stderr, " \"%s\"", *p);
		fprintf(stderr, "\n");
		return false;
	}

	if (query_for_p) {
		hxmc_t *tmp = NULL;

		printf("LDAP password: ");
		fflush(stdout);
		HX_getl(&tmp, stdin);
		if (tmp != NULL)
			ldapfuse_password = HX_strdup(HX_chomp(tmp));
		HXmc_free(tmp);
	}
	return true;
}

static int main2(int argc, char **argv)
{
	const struct HXdeque_node *node;
	int new_argc = 0, i;
	char **new_argv, *fsname;
	char buf[256];

	ldapfuse_extra_opts = HXdeque_init();
	if (ldapfuse_extra_opts == NULL) {
		perror("HXdeque_init");
		return EXIT_FAILURE;
	}

	if (!ldapfuse_get_options(&argc, const_cast3(const char ***, &argv)))
		return EXIT_FAILURE;

	if (argc < 3) {
		fprintf(stderr, "%s: need LDAP URI and mountpoint\n", *argv);
		return EXIT_FAILURE;
	}

	if (ldapfuse_init(argv[1]) < 0)
		return EXIT_FAILURE;

	new_argv = malloc(sizeof(char *) *
	           (5 + 2 * ldapfuse_extra_opts->items));
	new_argv[new_argc++] = argv[0];
	if (ldapfuse_foreground)
		new_argv[new_argc++] = "-f";
	fsname = ldapfuse_fsname_for_fuse(argv[1]);
	snprintf(buf, sizeof(buf), "-osubtype=ldapfuse,fsname=%s", fsname);
	new_argv[new_argc++] = buf;
	for (node = ldapfuse_extra_opts->first;
	     node != NULL; node = node->next)
	{
		new_argv[new_argc++] = "-o";
		new_argv[new_argc++] = node->ptr;
	}
	new_argv[new_argc++] = argv[2];
	new_argv[new_argc] = NULL;

	i = fuse_main(new_argc, new_argv, &ldapfuse_ops, NULL);
	free(new_argv);
	free(fsname);
	HXdeque_genocide(ldapfuse_extra_opts);
	ldapfuse_exit();
	return i;
}

int main(int argc, char **argv)
{
	int ret;

	ret = HX_init();
	if (ret <= 0) {
		fprintf(stderr, "HX_init: %s\n", strerror(errno));
		abort();
	}

	ret = main2(argc, argv);
	HX_exit();
	return ret;
}
